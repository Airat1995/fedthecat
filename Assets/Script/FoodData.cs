﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FoodData : MonoBehaviour {
	/// <summary>
	/// How much points user will get after hitting the bowl
	/// </summary>
	public int PointsForHitting;

	/// <summary>
	/// The item sprite that will be showed in listview
	/// </summary>
	public Sprite ItemSprite;

	/// <summary>
	/// Is current listview item checked
	/// </summary>
	public bool IsChecked;

	void OnCollisionEnter(Collision collision) {
		if (!collision.collider.tag.Equals ("Bowl"))
			return;
		var counter = GameObject.Find ("PointsCounter");
		int currentPoints; 
		if (!int.TryParse (counter.GetComponent<Text> ().text, out currentPoints))
			return;
		currentPoints += PointsForHitting;
		counter.GetComponent<Text> ().text = currentPoints.ToString();
		
	}
}
