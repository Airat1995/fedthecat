﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TicketDetermenation : MonoBehaviour {

	public QRCodeDecodeController qrCodeController;

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad (this);
		qrCodeController.onQRScanFinished += getResult;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void getResult(string codeData)
	{

		Debug.Log (codeData);
		if (codeData == "sometexthere")
		{
		    var loadGameScene = LoadGameScene();
		}
	}

    IEnumerator LoadGameScene()
    {
        Debug.Log ( "START Load Async" );
        var result = SceneManager.LoadSceneAsync ( "Gameplay", LoadSceneMode.Single );
        result.allowSceneActivation = true;

        while (!result.isDone)
        {
            Debug.Log ( "progress: " + result.progress );
            yield return new WaitForEndOfFrame ();
        }
        Debug.Log ( "YEAH Loaded Async" );
        GameObject.Find("Timer").GetComponent<Timer>().ResetTimer(100);
    }
}
