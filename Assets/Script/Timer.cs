﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof (Text))]
public class Timer : MonoBehaviour {

	private float _gameTime;

	private const string _elapsedTime = "Оставшееся время:";

    /// <summary>
    /// Text assigned to current timer
    /// </summary>
    private Text _timerText;

    void Start()
    {
        _timerText = gameObject.GetComponent<Text>();
    }

	bool IsStillGame() {
		return _gameTime > 0;
	}

	public void ResetTimer(float time) {
		_gameTime = time;
		
	}
	
	// Update is called once per frame
	void Update () {
		if (_gameTime <= 0)
			return;
		_gameTime -= Time.deltaTime;
		_timerText.text = string.Format (_elapsedTime + "\n {0:0}:{1:00}", _gameTime / 60, _gameTime % 60);
	}
}
