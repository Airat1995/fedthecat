﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

	/// <summary>
	/// The finger swipe start time. 
	/// </summary>
	private float _fingerStartTime = 0.0f;

	/// <summary>
	/// The finger swipe start position.
	/// </summary>
	private Vector2 _fingerStartPos = Vector2.zero;

	/// <summary>
	/// Is user swiping.
	/// </summary>
	private bool _isSwipe = false;

	/// <summary>
	/// Current AR camera's gameobject.
	/// </summary>
	public GameObject ArCamera;

	/// <summary>
	/// Initialize Z offset from camera;
	/// </summary>
	public float ZOffset;

	/// <summary>
	/// The minimum swipe distance.
	/// </summary>
	public float MinSwipeDist = 20.0f;
	/// <summary>
	/// The max swipe time in seconds.
	/// </summary>
	public float MaxSwipeTime = 2f;

	/// <summary>
	/// The swipe force.
	/// </summary>
	public float SwipeForce = 100f;
  
  	// Update is called once per frame
	void Update () {
		var active = GameObject.Find ("ListView").GetComponent<ListView> ().GetActiveListItem ();
		if (active == null)
			return;
		if (Input.touchCount > 0) {

			foreach (Touch touch in Input.touches) {
				var screenPos = new Vector3(touch.position.x, touch.position.y, ZOffset); 
				Vector3 worldPos = ArCamera.GetComponent<Camera>().ScreenToWorldPoint(screenPos);
				switch (touch.phase) {
				case TouchPhase.Began:
          /* this is a new touch */
					_isSwipe = true;
					_fingerStartTime = Time.time;
					_fingerStartPos = touch.position;
					break;
          
				case TouchPhase.Canceled:
          /* The touch is being canceled */
					_isSwipe = false;
					break;
          
				case TouchPhase.Ended:
					float gestureTime = Time.time - _fingerStartTime;
					float gestureDist = (touch.position - _fingerStartPos).magnitude;
					Debug.Log (gestureTime);

					if (_isSwipe && gestureTime < MaxSwipeTime && gestureDist > MinSwipeDist) {
						Vector2 direction = touch.position - _fingerStartPos;
						var selectedFruit = Instantiate (active, Vector3.zero, Quaternion.identity) as GameObject;
						selectedFruit.transform.position = worldPos;
						selectedFruit.GetComponent<Rigidbody> ().useGravity = true;
						selectedFruit.GetComponent<Rigidbody> ().AddForce (new Vector3 (direction.x/10, direction.y/10, (MaxSwipeTime - gestureTime)*SwipeForce/(MaxSwipeTime) ), ForceMode.Impulse);
					}
					break;
				}
			}
		}
	}
}