﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


[RequireComponent(typeof(RectTransform))]
public class ListView : MonoBehaviour, IDragHandler {

	private float _touchSize = 2.0f;

	/// <summary>
	/// The touch previous position.
	/// </summary>
	private PointerEventData _prevPosition;

	/// <summary>
	/// GameObject's name that contain TextView to present how much point will get user
	/// </summary>
	private string _pointsGameObject;

	/// <summary>
	/// GameObject's name there will be presented food
	/// </summary>
	private string _foodGameObject;

	private GameObject[] _instancieatedGameObject;

	/// <summary>
	/// The list item view;
	/// </summary>
	public GameObject ListItem;

	/// <summary>
	/// The list view items.
	/// </summary>
	public GameObject[] ListViewItems;

	// Use this for initialization
	void Start () {
		_instancieatedGameObject = new GameObject[ListViewItems.Length];
		float offset = ListItem.transform.GetChild (0).GetComponent<RectTransform> ().rect.width;

		int count = 0;
		for (int i = 0; i < ListViewItems.Length; i++) {
			_instancieatedGameObject[i] = Instantiate (ListItem, Vector2.zero, Quaternion.identity, gameObject.transform);
			_instancieatedGameObject[i].transform.localPosition = new Vector3 (i%2==0?offset * count:offset * -count, 0, 0);
			if (i % 2 == 0)
				count++;
			_instancieatedGameObject[i].transform.GetChild (1).GetComponent<Image> ().sprite = ListViewItems [i].GetComponent<FoodData> ().ItemSprite;
			_instancieatedGameObject[i].transform.GetChild (2).GetComponent<Text> ().text = ListViewItems [i].GetComponent<FoodData> ().PointsForHitting.ToString();
			foreach (Transform child in ListViewItems[i].transform) {
				
			}
		}
	}

	#region IDragHandler implementation

	public void OnDrag (PointerEventData eventData)
	{
		Rect listViewBound = gameObject.GetComponent<RectTransform> ().rect;
		float size = _instancieatedGameObject [0].transform.GetChild (0).GetComponent<RectTransform> ().sizeDelta.x/2;
		float positionLastEven = _instancieatedGameObject [_instancieatedGameObject.Length - 1].transform.localPosition.x;
		float positionLastOdd = _instancieatedGameObject [_instancieatedGameObject.Length - 2].transform.localPosition.x;


		if (_instancieatedGameObject.Length % 2 == 0 && positionLastEven - size > listViewBound.xMin && eventData.delta.x > 0)
			return;
		if (_instancieatedGameObject.Length % 2 == 0 && positionLastOdd + size < listViewBound.xMax && eventData.delta.x < 0)
			return;
		if (_instancieatedGameObject.Length % 2 != 0 && positionLastOdd - size > listViewBound.xMin && eventData.delta.x > 0)
			return;
		if (_instancieatedGameObject.Length % 2 != 0 && positionLastEven + size < listViewBound.xMax && eventData.delta.x < 0)
			return;
		foreach (var item in _instancieatedGameObject) {
			item.transform.localPosition += new Vector3(eventData.delta.x, 0, 0);
		}
	}

	#endregion

	public GameObject GetActiveListItem()
	{
		for (int i = 0; i < _instancieatedGameObject.Length; ++i) {
			if (_instancieatedGameObject [i].transform.GetChild (0).GetComponent<Image> ().enabled) {
				return ListViewItems [i];
			}
		}
		return null;
	}

	public void SetAllUnable(GameObject gameObject)
	{
		foreach (var item in _instancieatedGameObject) {
			if (item == gameObject)
				continue;
			item.transform.GetChild (0).GetComponent<Image> ().enabled = false;
		}
	}
}